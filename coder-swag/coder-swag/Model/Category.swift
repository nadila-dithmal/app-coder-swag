//
//  Category.swift
//  coder-swag
//
//  Created by Nadila Dithmal on 6/24/18.
//  Copyright © 2018 Nadila Dithmal. All rights reserved.
//

import Foundation

struct Category { //User defined type for ease of handling table data
    //Properties for the type, in variables, are private for setting and public for retrieving
    private(set) public var title: String
    private(set) public var imageName: String
    
    init(title: String, imageName: String){ //Initializer can set properties
        self.title = title
        self.imageName = imageName
    }
}
