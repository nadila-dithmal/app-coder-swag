//
//  CategoryCell.swift
//  coder-swag
//
//  Created by Nadila Dithmal on 6/24/18.
//  Copyright © 2018 Nadila Dithmal. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell { //Class for the cell

    //IBOutlets for cell elements
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    func updateViews (category: Category) { //Update view function for the cell, updates the cell
        categoryImage.image = UIImage(named: category.imageName) //sets the cell image to the imaged named as in the string grabbed by the data service
        categoryTitle.text = category.title //sets the cell title to the title string grabbed by the data service
    }

}
