//
//  DataService.swift
//  coder-swag
//
//  Created by Nadila Dithmal on 6/25/18.
//  Copyright © 2018 Nadila Dithmal. All rights reserved.
//

import Foundation

class DataService {
    static let instance = DataService() //Initializing DataService
    
    private let categories = [ //Array to simulate data
        Category(title: "SHIRTS", imageName: "shirts.png"),
        Category(title: "HOODIES", imageName: "hoodies.png"),
        Category(title: "HATS", imageName: "hats.png"),
        Category(title: "DIGITAL", imageName: "digital.png")
    ]
    
    private let hats = [
        Product(title: "Devslopes Logo Graphic Beanie", price: "$18", imageName: "hat01.png"),
        Product(title: "Devslopes Logo Hat Black", price: "$22", imageName: "hat02.png"),
        Product(title: "Devslopes Logo Hat White", price: "$22", imageName: "hat03.png"),
        Product(title: "Devslopes Logo Snapback", price: "$20", imageName: "hat04.png")
    ]
    
    private let hoodies = [
        Product(title: "Devslopes logo hoodie grey", price: "$32", imageName: "hoodie01.png"),
        Product(title: "Devslopes logo hoodie red", price: "$32", imageName: "hoodie02.png"),
        Product(title: "Devslopes hoodie grey", price: "$32", imageName: "hoodie03.png"),
        Product(title: "Devslopes hoodie black", price: "$32", imageName: "hoodie04.png")
    ]
    
    private let shirts = [
        Product(title: "Devslopes Logo Shirt Black", price: "$18", imageName: "shirt01.png"),
        Product(title: "Devslopes Badge Shirt Light Grey", price: "$18", imageName: "shirt02.png"),
        Product(title: "Devslopes Logo Shirt Red", price: "$18", imageName: "shirt03.png"),
        Product(title: "Hustle Delegate", price: "$18", imageName: "shirt04.png"),
        Product(title: "Kickflip Studios Black", price: "$18", imageName: "shirt05.png"),
        
    ]
    
    private let digitalGoods = [Product]()
    
    func getCategories() -> [Category] { //function which returns an array of categories
            return categories
    }
    
    func getProducts(forCategorytitle title: String) -> [Product]{
        switch title {
        case "SHIRTS":
            return shirts
        case "HATS":
            return hats
        case "HOODIES":
            return hoodies
        case "DIGITAL":
            return digitalGoods
        default:
            return shirts
        }
    }
    
}









