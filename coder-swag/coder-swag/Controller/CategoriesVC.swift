//
//  ViewController.swift
//  coder-swag
//
//  Created by Nadila Dithmal on 6/11/18.
//  Copyright © 2018 Nadila Dithmal. All rights reserved.
//

import UIKit

class CategoriesVC: UIViewController, UITableViewDataSource, UITableViewDelegate { //Import relevant Protocols
 
    @IBOutlet weak var categoryTable: UITableView! //IBOutlet for the table
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryTable.dataSource = self //set the data source for the table
        categoryTable.delegate = self   //set the delegate for the table
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { //returns and sets the number of rows for the table
        return DataService.instance.getCategories().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as? CategoryCell { //if let sequence, cast cell as of the view type CategoryCell
            let category = DataService.instance.getCategories()[indexPath.row]
            cell.updateViews(category: category) //calls update view function on the cell
            return cell
        } else { //if nil, return an empty cell
            return CategoryCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = DataService.instance.getCategories()[indexPath.row]
        performSegue(withIdentifier: "ProductsVC", sender: category)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let productsVC = segue.destination as? ProductsVC {
            
            //change later
            let barBtn = UIBarButtonItem()
            barBtn.title = ""
            navigationItem.backBarButtonItem = barBtn
            
            assert(sender as? Category != nil)
            productsVC.initProducts(category: sender as! Category)
            
        }
    }
}

